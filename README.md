# GPF DevSecOps Knowledge base

ยินดีต้อนรับเข้าสู่ศูนย์ข้อมูลด้าน DevSecOps ของกองทุนบำเหน็ขบำนาญข้าราชการ

# สารบัญ
--------------------------------------------------------------------------
ข้อมูลการวิเคราะห์ประเมิน แนะนำ เสริมข้อมูล เพื่อปรับเพิ่มประสิทธิภาพของกระบวนการ DevSecOps ของ กบข. (วันที่ 7 พย. 2565)
1. [Secure Pipeline Practice](../../../wikis/GPF-Secure-Pipeline-Practice)
2. [CI CD Pipeline ตามมาตรฐานสากล โดยแนะนำจาก INFINITY](../../../wikis/CI-CD-pipeline-recommend-by-INFINITY)
3. [CD Pipeline OnPrem ขึ้น OnCloud (AZURE) ปัจจุบันของ กบข.](../../../wikis/Existing-Pipeline-OnPrem-to-AZURE)
4. [วิเคราะห์กระบวนการ DevOps CI/CD Pipeline  ปัจจุบันของ กบข.](../../../wikis/analyze-GPF-Existing-CICD)\
    4.1. [วิเคราะห์ข้อดีและข้อด้อยของ Jenkins กบข.](../../../wikis/analyze-Pros-Cons-Jenkins)\
    4.2. [วิเคราะห์ข้อดีและข้อด้อยของ Azure Pipelines กบข.](../../../wikis/analyze-Pros-Cons-AZURE-Pipeline)\
    4.3. [วิเคราะห์การใช้งาน Git Repositorys กบข.](../../../wikis/analyze-Pros-Cons-Git-Repo)\
    4.4. [วิเคราะห์แต่ละขั้นตอน (Stage) ของแต่ละ Pipeline ณ ปัจจุบันของ กบข. กบข.](../../../wikis/analyze-GPF-CICD-Stage)
5. [คำแนะนำเพิ่ม Security ให้กับ DevOps CI/CD Pipeline กบข.](../../../wikis/Improve-Security-GPF-DevOps-CICD)\
	5.1. [วิเคราะห์ แนะนำการใช้งาน GitLab และ Jenkins](../../../wikis/analyze-Recommend-GitLab-Jenkins)\
	5.2. [วิเคราะห์ แนะนำด้านความปลอดภัยของ Credential](../../../wikis/Analyze-Recommend-Credential)\
	5.3. [วิเคราะห์ แนะนำด้านการใช้งาน Jenkins กับ Kubernetes เพื่อสร้าง Slave node](../../../wikis/Analyze-Recommend-Jenkins-K8s-for-slave)\
	5.4. [วิเคราะห์ แนะนำการจัดวาง Folder ของ Jenkins](../../../wikis/Analyze-Recommend-Jenkins-Folder)\
	5.5. [คำแนะนำการทำ Jenkinsfile ให้เป็น Template](../../../wikis/Analyze-Recommend-Jenkinsfile-template)\
	5.6. [อธิบาย CI Pipeline ที่แนะนำ](../../../wikis/Recommend-CI-Pipeline)\
	5.7. [อธิบาย CD Pipeline ที่แนะนำ](../../../wikis/Recommend-CD-Pipeline)\
	5.8. [อธิบาย Release Pipeline ที่แนะนำ](../../../wikis/Recommend-Release-Pipeline)\
	5.9. [คำแนะนำการทำ Pipeline Monitoring](../../../wikis/Recommend-Pipeline-Monitoring)
6. [บทวิเคราะห์คำแนะนำเพิ่ม Security ให้กับ DevOps CI/CD Pipeline กบข.]()\


--------------------------------------------------------------------------
